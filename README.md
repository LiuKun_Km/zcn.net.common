# ZCN.NET.Common

#### 介绍
【ZCN.NET.Common】是基于.NET Standard 技术封装的二次开发通用类库。包含了常用工具类：缓存、开发组件、常量、依赖、实体、枚举、异常、扩展类、Http通讯、日志、加密解密、文件IO、日期时间等类。

#### 软件架构
【ZCN.NET.Common】是基于.NET Standard 技术封装的二次开发通用类库。同时支持以下目标框架： .NET4.0、.NET4.5、.NET4.5.1、.NET4.5.2、.NET4.6、.NET4.6.1、.NET4.6.2、.NET4.7、.NET4.7.1、.NET4.7.2、.NET4.8、.NET Core 3.0、.NET Core 3.1、.NET 5.0。可以在Windows、Linux、MaxOS系统上开发部署。

【ZCN.NET.CommonX】是基于.NET 5.0 技术封装的二次开发通用类库。支持全场景业务开发与跨平台部署。<br/>
![输入图片说明](https://images.gitee.com/uploads/images/2020/1029/173504_fd77ab1e_1273526.png "屏幕截图.png")


#### 安装教程

1、本项目使用Visual Studio 2019创建，建议使用VS2019打开该项目。
具体版本信息如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/1029/144841_383b5fa8_1273526.png "屏幕截图.png")
<br/>如果您使用低版本的 Visual Studio，请自行创建解决方案，然后手动添加【ZCN.NET.Common】、【ZCN.NET.CommonX】项目即可。
<br/>2、【ZCN.NET.Common】是使用 .NET Standard 技术封装，且输出多目标框架版本程序集。
<br/>3、【ZCN.NET.CommonX】是使用 .NET5.0 技术封装。
<br/>4、【ZCN.NET.Common】、【ZCN.NET.CommonX】项目引用了第三方组件，编译项目时会自动下载。
<br/>5、编译成功后即可进行集成开发。